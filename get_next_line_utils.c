/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kreginal <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/03 16:35:48 by kreginal          #+#    #+#             */
/*   Updated: 2021/12/05 20:01:17 by kreginal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "get_next_line.h"

// Функция ft_strlen вычисляет количество символов в строке до первого
// Вхождения символа конца строки. При этом символ конца строки не входит
// В подсчитанное количество символов.
int	ft_strlen(char *s)
{
	int	i;

	i = 0;
	while (*s++)
		i++;
	return (i);
}

// Функция ft_strchr ищет первое вхождения символа, код которого 
// Указан в аргументе c, в строке, на которую указывает аргумент s.
char	*ft_strchr(char *s, int c)
{
	if (!s)
		return (0);
	while (*s)
	{
		if (*s == (char) c)
			return (s);
		s++;
	}
	if (c == 0)
		return (s);
	return (NULL);
}

// Функция возвращает указатель на массив, в который добавлена строка.
static void	ft_strcat(char *dest, char *src, char *str)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (dest[i] != '\0')
	{
		str[i] = dest[i];
		i++;
	}
	while (src[j] != '\0')
	{
		str[i + j] = src[j];
		j++;
	}
	str[i + j] = '\0';
}

// Чуть более уникальная функция ft_strjoin, повторяет поведение обычной, но
// После объединения двух строк и аллоцирования использует ft_strcat для
// Возращения указателя на массив и освобождения памяти под новую встроку.
char	*ft_strjoin(char *s1, char *s2)
{
	char	*str;
	int		total;

	if (!s1)
	{
		s1 = malloc(sizeof(char) * 1);
		s1[0] = '\0';
	}
	total = ft_strlen(s1) + ft_strlen(s2);
	str = malloc(sizeof(char) * (total + 1));
	if (!str)
		return (NULL);
	ft_strcat(s1, s2, str);
	free(s1);
	return (str);
}
