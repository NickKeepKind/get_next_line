/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_bonus.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kreginal <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 14:03:08 by kreginal          #+#    #+#             */
/*   Updated: 2021/12/08 14:15:30 by kreginal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

// Header в BONUS части такой же, только название меняется.
#ifndef GET_NEXT_LINE_BONUS_H
# define GET_NEXT_LINE_BONUS_H

# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 42
# endif

# include <stdlib.h>
# include <unistd.h>

int		ft_strlen(char *s);
char	*ft_strchr(char *s, int c);
char	*ft_strjoin(char *s1, char *s2);

char	*ft_read_lstr(char *buff, int fd);
char	*ft_get_line(char *buff);
char	*ft_new_line(char *buff);

char	*get_next_line(int fd);

#endif
