                                           ############**Kreginal**##########
                                           #          Get_Next_line         #
                                           ##########**NickKeepKind**########

My function that will store, in the parameter “line”, a line that has been read from the given file descriptor. From Shcool 21, Russia.

**#ATTENTION**

**Внимательно прочитайте Subject прикрепленный к репозиторию, а так же сверьте версию Norminette перед тем как отправлять.**

**#Comments**

Проект не сложный, от вас требуется лишь внимательность, интерес и трудолюбие. 
Из важного:
- **1** - Разберитесь, что от вас требуется в проекте.
- **2** - При написании проекта постоянно проверяйте его через собственые txt файлы. Так-же очень полезно использовать **дебагеры** в Visual Studio и CLion, частенько они вас будут спасать. _(я оставил свой main.c обязательно **используйте или напишите свой**!)_
- **3** - Обязательно, всегда, следите за буффером, что-б знать что именно вы копируете/выделяете/сохраняете и т.д.
- **4** - Рисуйте схемы, чертите, пишите себе на бумажке, делайте все дабы понимать последовательность ваших функций и в целом алгоритма.
- **5** - **Peer2Peer** и **заимствуете идеи**, а не коды ;)


**#Useful**

- **1** - https://www.youtube.com/watch?v=kbk0a1IxDNU
- **2** - https://www.youtube.com/watch?v=eBa0JwamKbA
- **3** - https://www.youtube.com/watch?v=toXltIcFX0Q&t=0s
- **4** - https://www.youtube.com/watch?v=Edf98rf3Z3s
- **5** - https://velog.io/@ljiwoo59/getnextline

**#Tests**

Для тестировании я использовал следующие, уже существущие тестеры:

1. https://github.com/Tripouille/gnlTester
1. https://github.com/Hellio404/Get_Next_Line_Tester


