/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kreginal <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 13:27:03 by kreginal          #+#    #+#             */
/*   Updated: 2021/12/08 13:27:06 by kreginal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "get_next_line_bonus.h"

// Все функции кроме get_next_line остались такими же как в основоной части.
char	*ft_read_buff(char *buff, int fd)
{
	char	*box;
	int		read_byte;

	box = malloc(sizeof(char) * (BUFFER_SIZE + 1));
	if (!box)
		return (NULL);
	read_byte = 1;
	while (read_byte && !(ft_strchr(buff, '\n')))
	{
		read_byte = read(fd, box, BUFFER_SIZE);
		if (read_byte == -1)
		{
			free(box);
			return (NULL);
		}
		box[read_byte] = '\0';
		buff = ft_strjoin(buff, box);
	}
	free(box);
	return (buff);
}

char	*ft_get_line(char *buff)
{
	char	*str;
	int		len;
	int		i;

	len = 0;
	if (buff[0] == '\0')
		return (NULL);
	while (buff[len] != '\0' && buff[len] != '\n')
		len++;
	if (buff[len] == '\n')
		len++;
	str = malloc(sizeof(char) * (len + 1));
	if (!str)
		return (NULL);
	i = 0;
	while (i < len)
	{
		str[i] = buff[i];
		i++;
	}
	str[i] = '\0';
	return (str);
}

char	*ft_new_line(char *buff)
{
	char	*new_str;
	int		len;
	int		i;

	len = 0;
	while (buff[len] != '\n' && buff[len] != '\0')
		len++;
	if (buff[len] == '\0')
	{
		free(buff);
		return (NULL);
	}
	i = 0;
	new_str = malloc(sizeof(char) * (ft_strlen(buff) - len + 1));
	if (!new_str)
		return (NULL);
	while (buff[len++] != '\0')
		new_str[i++] = buff[len];
	new_str[i] = '\0';
	free(buff);
	return (new_str);
}

// Т.к мы работаем с массивами, мы можем просто поменять 
// Статичную переменную и добавить возможность прочтения
// Дополнительных файлов. Думаю тут все максимально понятно :)
char	*get_next_line(int fd)
{
	static char	*buff[1024];
	char		*line;

	if (fd < 0 || BUFFER_SIZE <= 0)
		return (NULL);
	buff[fd] = ft_read_buff(buff[fd], fd);
	if (!buff[fd])
		return (NULL);
	line = ft_get_line(buff[fd]);
	buff[fd] = ft_new_line(buff[fd]);
	return (line);
}
