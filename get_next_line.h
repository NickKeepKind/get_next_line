/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kreginal <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/03 16:13:05 by kreginal          #+#    #+#             */
/*   Updated: 2021/12/05 20:00:17 by kreginal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

// Собственная переменная для измненния буфера удобно.
# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 42
# endif

// Нужные нам либы
# include <stdlib.h>
# include <unistd.h>

// Либовские функции
int		ft_strlen(char *s);
char	*ft_strchr(char *s, int c);
char	*ft_strjoin(char *s1, char *s2);

// Дополнительные к GNL
char	*ft_read_lstr(char *buff, int fd);
char	*ft_get_line(char *buff);
char	*ft_new_line(char *buff);

// Сам Get_Next_LIne
char	*get_next_line(int fd);

#endif
