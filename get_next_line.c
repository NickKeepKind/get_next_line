/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kreginal <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/03 16:22:48 by kreginal          #+#    #+#             */
/*   Updated: 2021/12/05 16:04:30 by kreginal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "get_next_line.h"

// Функция читает текстовый файл по буферу защищая нас от нежелательных ошибок
// И в конечном итоге возращая нам прочитанный буффер.
char	*ft_read_buff(char *buff, int fd)
{
	char	*box;
	int		read_byte;

	box = malloc(sizeof(char) * (BUFFER_SIZE + 1));
	if (!box)
		return (NULL);
	read_byte = 1;
	while (read_byte && !(ft_strchr(buff, '\n')))
	{
		read_byte = read(fd, box, BUFFER_SIZE);
		if (read_byte == -1)
		{
			free(box);
			return (NULL);
		}
		box[read_byte] = '\0';
		buff = ft_strjoin(buff, box);
	}
	free(box);
	return (buff);
}

// Избавляем строку от остатков и мусора, выделяя только то, что нам нужно.
char	*ft_get_line(char *buff)
{
	char	*str;
	int		len;
	int		i;

	len = 0;
	if (buff[0] == '\0')
		return (NULL);
	while (buff[len] != '\0' && buff[len] != '\n')
		len++;
	if (buff[len] == '\n')
		len++;
	str = malloc(sizeof(char) * (len + 1));
	if (!str)
		return (NULL);
	i = 0;
	while (i < len)
	{
		str[i] = buff[i];
		i++;
	}
	str[i] = '\0';
	return (str);
}

// Записываем строку и делаем возможным запись последующих строк.
char	*ft_new_line(char *buff)
{
	char	*new_str;
	int		len;
	int		i;

	len = 0;
	while (buff[len] != '\n' && buff[len] != '\0')
		len++;
	if (buff[len] == '\0')
	{
		free(buff);
		return (NULL);
	}
	i = 0;
	new_str = malloc(sizeof(char) * (ft_strlen(buff) - len + 1));
	if (!new_str)
		return (NULL);
	while (buff[len++] != '\0')
		new_str[i++] = buff[len];
	new_str[i] = '\0';
	free(buff);
	return (new_str);
}

// Собственно сама функция, которая сохранит в параметре “remaining_str” 
// Строку, считанную из данного файлового дескриптора
char	*get_next_line(int fd)
{
	static char	*remaining_str;
	char		*line;

	if (fd < 0 || BUFFER_SIZE <= 0)
		return (0);
	remaining_str = ft_read_buff(remaining_str, fd);
	if (!remaining_str)
		return (NULL);
	line = ft_get_line(remaining_str);
	remaining_str = ft_new_line(remaining_str);
	return (line);
}
