/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kreginal <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 14:12:49 by kreginal          #+#    #+#             */
/*   Updated: 2021/12/08 14:12:52 by kreginal         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "get_next_line_bonus.h"
#include "get_next_line.h"
#include <stdio.h>
#include <fcntl.h>

// MANDATORY PART
/*
int main(void)
{
	int fd = open("text.txt", O_RDONLY);

	printf("%s", get_next_line(fd));
	printf("%s", get_next_line(fd));
	printf("%s", get_next_line(fd));
	printf("%s", get_next_line(fd));
}
*/
// BONUS PART
/*
int main (void)
{
	int fd = open("text.txt", O_RDONLY);
	int fd1 = open("text1.txt", O_RDONLY);
    int fd2 = open("text2.txt", O_RDONLY);
	int fd3 = open("text3.txt", O_RDONLY);
    int i = 0;

	while(i++ < 10)
	{
		printf("%s\n", get_next_line(fd));
		printf("%s\n", get_next_line(fd1));
        printf("%s\n", get_next_line(fd2));
		printf("%s\n", get_next_line(fd3));
		getchar();
	}
	return (0);
}
*/